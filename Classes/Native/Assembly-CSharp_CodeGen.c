﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void EndTrigger::OnTriggerEnter()
extern void EndTrigger_OnTriggerEnter_mB0D6D2E23C37829FBAAC9A09F404823F3671861A (void);
// 0x00000002 System.Void EndTrigger::.ctor()
extern void EndTrigger__ctor_m396FC2C4F7B51D28A3AD9DBE011F3B509CD2AE3F (void);
// 0x00000003 System.Void GameManage::CompleteLevel()
extern void GameManage_CompleteLevel_mF36BE646CD265F4215CA6D29C4343D77484DF1F7 (void);
// 0x00000004 System.Void GameManage::EndGame()
extern void GameManage_EndGame_m420E730CEB6223944F03CD7EE784AC6950A0EFAC (void);
// 0x00000005 System.Void GameManage::Restart()
extern void GameManage_Restart_mE26A35C6B62D4EB30F085CCB844ED3E3740A1D50 (void);
// 0x00000006 System.Void GameManage::.ctor()
extern void GameManage__ctor_mC37280FAA0CA340D94873873B847A57222D614E8 (void);
// 0x00000007 System.Void Move::Start()
extern void Move_Start_m11F7E13E9C92C9009E4150FA83973BB9A5E9FB4B (void);
// 0x00000008 System.Void Move::FixedUpdate()
extern void Move_FixedUpdate_m694B84E94829BA742775A55B8A49BBBE880F7F15 (void);
// 0x00000009 System.Void Move::.ctor()
extern void Move__ctor_m7D963099B75667E43E14FB30057A669AB0EA625C (void);
// 0x0000000A System.Void PlayerCollision::OnCollisionEnter(UnityEngine.Collision)
extern void PlayerCollision_OnCollisionEnter_m05D276A7306FAA907C1847F934E9CFEAACA13EC5 (void);
// 0x0000000B System.Void PlayerCollision::.ctor()
extern void PlayerCollision__ctor_mB6CC65E3E3B37769F12E0283C5B014269A9065E8 (void);
// 0x0000000C System.Void Score::Update()
extern void Score_Update_mB696B161F91F1AE220BCD4F47028DE039D9697D2 (void);
// 0x0000000D System.Void Score::.ctor()
extern void Score__ctor_mEE9186D20D9B28A735262B29AB6E8D9FF1380FB6 (void);
// 0x0000000E System.Void followPlayer::Update()
extern void followPlayer_Update_mE8D9FB3AC80CC069B0A0CB222963254E5B70FB83 (void);
// 0x0000000F System.Void followPlayer::.ctor()
extern void followPlayer__ctor_m5002B92457D07A9CFC6A85F3B741C743A11A72CA (void);
static Il2CppMethodPointer s_methodPointers[15] = 
{
	EndTrigger_OnTriggerEnter_mB0D6D2E23C37829FBAAC9A09F404823F3671861A,
	EndTrigger__ctor_m396FC2C4F7B51D28A3AD9DBE011F3B509CD2AE3F,
	GameManage_CompleteLevel_mF36BE646CD265F4215CA6D29C4343D77484DF1F7,
	GameManage_EndGame_m420E730CEB6223944F03CD7EE784AC6950A0EFAC,
	GameManage_Restart_mE26A35C6B62D4EB30F085CCB844ED3E3740A1D50,
	GameManage__ctor_mC37280FAA0CA340D94873873B847A57222D614E8,
	Move_Start_m11F7E13E9C92C9009E4150FA83973BB9A5E9FB4B,
	Move_FixedUpdate_m694B84E94829BA742775A55B8A49BBBE880F7F15,
	Move__ctor_m7D963099B75667E43E14FB30057A669AB0EA625C,
	PlayerCollision_OnCollisionEnter_m05D276A7306FAA907C1847F934E9CFEAACA13EC5,
	PlayerCollision__ctor_mB6CC65E3E3B37769F12E0283C5B014269A9065E8,
	Score_Update_mB696B161F91F1AE220BCD4F47028DE039D9697D2,
	Score__ctor_mEE9186D20D9B28A735262B29AB6E8D9FF1380FB6,
	followPlayer_Update_mE8D9FB3AC80CC069B0A0CB222963254E5B70FB83,
	followPlayer__ctor_m5002B92457D07A9CFC6A85F3B741C743A11A72CA,
};
static const int32_t s_InvokerIndices[15] = 
{
	1128,
	1128,
	1128,
	1128,
	1128,
	1128,
	1128,
	1128,
	1128,
	966,
	1128,
	1128,
	1128,
	1128,
	1128,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	15,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
